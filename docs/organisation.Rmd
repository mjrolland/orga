---
title: "Work organisation Matthieu Feb 2020 - Feb 2021"
author: "M. Rolland"
date: "17/02/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)
```

```{r}
library(tidyverse)
library(DiagrammeR)
library(pander)
```

</br>

* List of tasks with approximate duration for the period feb 2020 to feb 2021:

```{r}
tab <- tibble::tribble(
  ~supervisor, ~task, ~detail, ~duration,
  "Claire", "Phtalates paper", "", "10d",
  "Remy", "Growth analyses", "", "15d",
  "Remy", "Prepare ultrasound data", "", "5d",
  "Claire", "Babylab analyses", "", "35d",
  "Claire", "Supervise Andres", "", "15d",
  "Claire", "ED - TH - Neuro mediation", "", "30d",
  "Extra", "Reproducibility project", "", "5d",
  "Extra", "Write sepages pipelin doc", "", "3d",
  "Extra", "Exposome summer school", "", "4d",
  "Extra", "Rencontres R Paris", "", "3d",
  "Extra", "R in Grenoble // R - Python", "","5d",
  "Johanna", "Premat VS temp", "", "6m"
) %>%
  mutate(supervisor = factor(supervisor, levels = c("Claire", "Remy", "Johanna", "Extra"))) %>%
  arrange(supervisor)

pander(tab)
```

</br>

* Timeline for the period feb 2020 - feb 2021:

```{r out.width = "100%"}
DiagrammeR::mermaid("
gantt
dateFormat  YYYY-MM-DD
title Organisation Feb 2020 - Feb 2021

section Claire
Write Sepages pipeline guide  :active,        claire_1,   2020-02-17, 4d
Phthalates paper              :active,        claire_2,   after claire_1, 20d
Babylab analyses              :active,        claire_3,   after claire_2, 2020-05-15
Babylab analyses              :               claire_4,   2020-09-01, 2020-12-31
Supervise andres              :active,        claire_5,   2020-02-17, 2020-06-30
ED-TH-neuro mediation         :               claire_6,   2020-09-01, 2021-02-15

section Johanna
Premat VS temp                :               johanna_1,  2020-05-15, 2020-07-31
Premat VS temp                :               johanna_2,  2020-09-01, 2021-02-15

section Remy
Growth analyses + paper       :active,        remy_1,     after claire_1, 2020-04-15

section Data Management
Remontee donnees echo         :               dm_1,       after remy_1, 10d

section Extras
Exposome summer school        :               extra_1,    2020-05-04, 2020-05-08
Rencontres R Paris            :               extra_2,    2020-07-15, 2020-07-17
Reproducible research         :               extra_3,    2020-04-01, 2020-04-14
R in Grenoble // R - Python   :               extra_4,    2020-03-15, 2020-03-26
")
```

