---
title: "Suivi temps travail"
author: "M. Rolland"
date: "`r Sys.Date()`"
output: 
  html_document:
    number_sections: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, message = FALSE)
```

```{r}
library(tidyverse)
library(lubridate)
library(knitr)
library(kableExtra)
library(plotly)
```

```{r}
start_date <- ymd("2020-02-17")
end_date <- Sys.Date()
report_interval <- interval(start = start_date, end = end_date)
```

```{r}
tab <- tribble(
  ~supervisor, ~task, ~project, ~date, ~time,
  "Johanna", "Work organisation", "extra", "2020-02-17", 1,
  "Claire", "Meeting with Solene", "ed_th_agnostic", "2020-02-17", 1,
  "Claire", "Phenols phthalates data management", "data_management", "2020-02-17", 4,
  "Valerie", "immuno data management", "data_management", "2020-02-18", 6,
  "Other", "Mardi de l'iab", "extra", "2020-02-18", 1.5,
  "Other", "ITA codir", "extra", "2020-02-18", 1,
  "Valerie", "immuno data management", "data_management", "2020-02-19", 8,
  "Sarah", "point sarah data viz", "data_vis", "2020-02-19", 0.5,
  "Other", "R in grenoble", "extra", "2020-02-20", 2,
  "Claire", "biblio ed th", "ed_th_agnostic", "2020-02-20", 2,
  "Claire", "point andres", "ed_th_agnostic", "2020-02-20", 2,
  "Valerie", "immuno data management", "data_management", "2020-02-21", 8,
  "Remy", "analyses croissance", "bpa_bps_growth", "2020-02-24", 8,
  "Claire", "biblio andres", "ed_th_agnostic", "2020-02-27", 8,
  "Claire", "write pipeline guide", "data_management", "2020-03-03", 8,
  "Claire", "write pipeline guide", "data_management", "2020-03-04", 4,
  "Remy", "analyses croissance", "bpa_bps_growth", "2020-03-04", 4,
  "Valerie", "immuno data management", "data_management", "2020-03-05", 8,
  "Other", "presentation r python", "RUG", "2020-03-04", 4,
  "Valerie", "immuno data management", "data_management", "2020-03-09", 2,
  "Other", "Omics talk imag", "extra", "2020-03-09", 2,
  "Remy", "analyses croissance", "bpa_bps_growth", "2020-03-11", 3,
  "Claire", "RDV David", "babylab", "2020-03-11", 3,
  "Claire", "Papier phthalates", "phthalates_descriptive", "2020-03-11", 2,
  "Other", "Réunion d'équipe",  "extra", "2020-03-12", 2,
  "Claire", "Papier phthalates", "phthalates_descriptive", "2020-03-12", 6,
  "Claire", "Rapport mensuel", "extra", "2020-03-13", 2,
  "Other", "Doc covid 19", "extra", "2020-03-13", 2, 
  "Claire", "Papier phthalates", "phthalates_descriptive", "2020-03-13", 4,
  "Claire", "Papier phthalates", "phthalates_descriptive", "2020-03-16", 4,
  "Claire", "Papier phthalates", "phthalates_descriptive", "2020-03-17", 7,
  "Claire", "Papier phthalates", "phthalates_descriptive", "2020-03-18", 4,
  "Claire", "Papier phthalates", "phthalates_descriptive", "2020-03-19", 2,
  "Claire", "Code phthalates", "phthalates_descriptive", "2020-03-19", 2,
  "Claire", "Point andres", "ed_th_agnostic", "2020-03-19", 1,
  "Claire", "Phenols proofs", "penols_descriptive", "2020-03-19", 1,
  "Claire", "Phenols proofs", "penols_descriptive", "2020-03-20", 2,
  "Claire", "Phenols proofs", "penols_descriptive", "2020-03-22", 1,
  "Claire", "biblio andres", "ed_th_agnostic", "2020-03-24", 5,
  "Other", "MOOC RR", "extra", "2020-03-25", 2,
  "Remy", "analyses croissance", "bpa_bps_growth", "2020-03-25", 2,
  "Other", "mails, organise meeting, etc", "extra", "2020-03-26", 2,
  "Remy", "analyses croissance", "bpa_bps_growth", "2020-03-26", 6,
  "Remy", "analyses croissance", "bpa_bps_growth", "2020-03-27", 5,
  "Other", "team meeting", "extra", "2020-03-27", 1,
  "Claire", "Papier phthalates", "phthalates_descriptive", "2020-04-13", 8,
  "Claire", "Finalisation analyses descriptives", "babylab", "2020-04-14", 8,
  "Claire", "biblio bkmr, point andres, etc", "ed_th_agnostic", "2020-04-27", 9,
  "Claire", "biblio, rédaction plan d'analyse", "babylab", "2020-04-28", 9,
  "Other", "mails, team meeting", "extra", "2020-05-04", 2,
  "Claire", "integrer comms papier phthal", "phthalates_descriptive","2020-05-04", 2,
  "Claire", "point andres", "ed_th_agnostic", "2020-05-04", 2,
  "Claire", "finaliser selection vars", "babylab", "2020-05-04", 2,
  "Johanna", "do DAG", "preterm_temp", "2020-05-05", 8,
  "Claire", "bkmr", "ed_th_agnostic", "2020-05-12", 6,
  "Claire", "Reprendre papier, finir code pour review", "phthalates_descriptive", "2020-05-12", 3,
  "Claire", "Finir papier", "phthalates_descriptive", "2020-05-13", 8,
  "Remy", "Team presentation", "bpa_bps_growth", "2020-05-15", 4,
  "Remy", "Team presentation", "bpa_bps_growth", "2020-05-18", 4,
  "Claire", "bkmr", "ed_th_agnostic", "2020-05-19", 2,
  "Claire", "bkmr", "ed_th_agnostic", "2020-05-20", 1,
  "Johanna", "start to look at data", "preterm_temp", "2020-05-20", 4,
  "Other", "e-mails etc", "extra", "2020-05-22", 1,
  "Johanna", "start data description", "preterm_temp", "2020-05-22", 2,
  "Claire", "Andres presentation", "ed_th_agnostic", "2020-05-22", 1,
  "Claire", "Andres presentation", "ed_th_agnostic", "2020-05-25", 1,
  "Other", "Team meeting", "extra", "2020-05-25", 1,
  "Other", "breastfeeding data management", "data_management", "2020-05-25", 6,
  "Other", "breastfeeding data management", "data_management", "2020-05-27", 4,
  "Johanna", "data exploration", "preterm_temp", "2020-05-27", 4,
  "Sarah", "prepare data viz for sepages participants", "data_vis", "2020-05-28", 4,
  "Johanna", "data exploration", "preterm_temp", "2020-05-28", 4,
  "Johanna", "meeting w/ JL + Ar.G", "preterm_temp", "2020-05-29", 2,
  "Other", "e-mails, dplyr 1.0.0 etc", "extra", "2020-06-02", 1,
  "Claire", "Andres presentation", "ed_th_agnostic", "2020-06-02", 1,
  "Johanna", "DAG, biblio", "preterm_temp", "2020-06-02", 3,
  "Sarah", "data viz sepages", "data_vis", "2020-06-02", 3,
  "Claire", "Andres soutenance", "ed_th_agnostic", "2020-06-08", 1,
  "Other", "Team meeting", "extra", "2020-06-08", 1,
  "Other", "e-mails", "extra", "2020-06-09", 2,
  "Johanna", "DAG, meeting ariane, data description", "preterm_temp", "2020-06-09", 6,
  "Johanna", "data description", "preterm_temp", "2020-06-10", 4,
  "Other", "MOOC RR", "extra", "2020-06-10", 4,
  "Other", "MOOC RR", "extra", "2020-06-12", 4,
  "Other", "Team meeting, emails", "extra", "2020-06-15", 2,
  "Sarah", "data viz sepages", "data_vis", "2020-06-15", 4,
  "Claire", "nouvelle base karine", "babylab", "2020-06-15", 2,
  "Sarah", "data viz sepages", "data_vis", "2020-06-16", 4,
  "Johanna", "meeting + data management", "preterm_temp", "2020-06-16", 4,
  "Other", "R conf erum", "preterm_temp", "2020-06-17", 4,
  "Johanna", "data management", "preterm_temp", "2020-06-17", 4,
  "Other", "R conf erum", "preterm_temp", "2020-06-18", 4,
  "Johanna", "get exp data and start to explore", "preterm_temp", "2020-06-18", 4,
  "Other", "R conf erum", "preterm_temp", "2020-06-19", 4,
  "Johanna", "explore expos", "preterm_temp", "2020-06-19", 4,
  "Remy", "relance analyses", "bpa_bps_growth", "2020-06-21", 2,
  "Other", "mails etc", "extra", "2020-06-21", 1,
  "Johanna", "explore expos", "preterm_temp", "2020-06-21", 5,
  "Johanna", "explore expos", "preterm_temp", "2020-06-22", 8,
  "Johanna", "explore expos", "preterm_temp", "2020-06-23", 8,
  "Johanna", "explore expos, prepare meeting dnlm biblio", "preterm_temp", "2020-06-25", 8,
  "Johanna", "prepare meeting, meeting", "preterm_temp", "2020-06-26", 4,
  "Johanna", "explore expos", "preterm_temp", "2020-06-28", 8,
  "Johanna", "explore expos", "preterm_temp", "2020-06-29", 8,
  "Johanna", "explore expos", "preterm_temp", "2020-06-30", 8,
  "Johanna", "explore expos", "preterm_temp", "2020-07-01", 8,
  "Johanna", "biblio", "preterm_temp", "2020-07-02", 8,
  "Johanna", "biblio", "preterm_temp", "2020-07-03", 2,
  "Sarah", "data viz sepages", "data_vis", "2020-07-03", 6,
  "Johanna", "biblio", "preterm_temp", "2020-07-06", 8,
  "Johanna", "biblio", "preterm_temp", "2020-07-07", 8,
  "Johanna", "biblio", "preterm_temp", "2020-07-08", 8,
  "Johanna", "plan d'analyse", "preterm_temp", "2020-07-09", 8,
  "Johanna", "biblio/exploration", "preterm_temp", "2020-07-10", 8,
  "Remy", "update analysis for SHALCOH report", "bpa_bps_growth", "2020-07-15", 3
  
) 

```

Suivi du temps de travail entre le **`r start_date`** et le **`r end_date`**

Note: temps de lecture/écriture e-mails et autres activités du quotidien pas systématiquement enregistrés.

# Temps cumulé par projet

```{r out.width="50%"}
tab %>%
  filter(ymd(date) %within% report_interval) %>%
  group_by(project) %>%
  summarise(total_time = sum(time)) %>%
  ggplot(aes(y = total_time,
             x = project,
             fill = project)) +
  geom_histogram(stat = "identity") +
  see::theme_modern() +
  see::scale_fill_material_d() +
  ggtitle("by project") +
  theme(legend.position = "none") +
  coord_flip()
```

# Temps cumulé par chercheur

```{r out.width="50%"}
tab %>%
  filter(ymd(date) %within% report_interval) %>%
  group_by(supervisor) %>%
  summarise(total_time = sum(time)) %>%
  ggplot(aes(y = total_time,
             x = supervisor,
             fill = supervisor)) +
  geom_histogram(stat = "identity") +
  see::theme_modern() +
  coord_flip() +
  ggtitle("by supervisor") +
  see::scale_fill_material_d() +
  theme(legend.position = "none")
```

# Détail

```{r fig.height = 3, fig.width = 9}
p <- ggplot(tab, aes(x = ymd(date),
                     y = time,
                     fill = project)) +
  geom_bar(stat = "identity") +
  xlim(c(start_date, end_date)) +
  see::theme_lucid() +
  scale_x_date(date_labels = "%b") +
  see::scale_fill_material_d() +
  ggtitle(str_c("Daily activity from ", start_date, " to ", end_date)) 

ggplotly(p)
```

```{r}
tab %>%
  kable(caption = "total work detail") %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive")) %>%
  scroll_box(height = "300px")
```

