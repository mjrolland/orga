---
title: "Progress report"
author: "M. Rolland"
date: "13/03/2020"
output: 
  html_document
---

Nb d'heures passées sur chaque projet entre le 17-02-2020 et le 12-03-2020:

<img src="../figs/tps_travail_2020-03-12.png" width="600">


Bilan: j'ai encore passé énormément de tps sur les données cytokines ce qui m'a mis en retard sur tout le reste. J'ai enfin fini à priori et ai commencé à rattraper le retard. La prochaine fois bien prendre en compte le temps que prennent ces opérations de data management... 

Extra = réunions, talks, etc.

Vacs non icluses.

# Data manamgement

En plus de la préparation des données cyto j'ai rédigé le doc sur la correction des données si tu veux y jeter un oeil:

https://bookdown.org/mj_rolland/sepages_pipeline_doc/

# Phenols

En attente du retour des reviewers, tu as du nouveau?

# Phthalates

Je viens juste de commencer à reprendre. La je met l'ordre dans les figures et tables. Que penses-tu de cela pour les corrélations (à la place des histogrammes que tu n'avais pas aimé et les points que je trouvais moche):

<img src="../figs/correlations_dotchart.png" width="600">


Autre question: pour la figure de comparaison entre cohortes de grossess: tu as mis les niveaux sepages a T3, ct un choix volontaire? ne devrait-on pas combiner toutes les mesures grossesse?

# Babylab

J'ai vu David mercredi 11/03/2020. Je n'avais pas avancé sur ces analyses depuis ton départ. Il m'a éclairé sur de nombreux points (bcp de notes, je ne sais pas si ça t'intéresse). Notamment quelles variables retenir entre les corrélées, et la signification des scores un peu plus élaborés (réaction à la nouveauté par ex).

**Next steps:**

* Finaliser DAG
* Correlation avec scores neuro
* Finaliser doc descriptif
* Commencer à rédiger plan d'analyse

# Andres

Tout avance bien sur la partie ED/TH. Je n'y passe pas bcp de tps en, dehors de notre point hebdo. 

On a quasi fini le doc descriptif. Après notre point de toute à l'heure il devait m'envoyer une version mise à jour (avec le N final dans toutes les tables et figs) mais il a oublié de commit (synchroniser sur git). Je préfère attendre qu'il le fasse demain matin avant de te l'envoyer. Mais si tu veux on peut t'envoyer la version actuelle.

## Finalisation du modele

Pour la finalisation du modèle nous avons regardé en plus des vars de Dorothy les 4 vars protocole suivantes:

* tps de congelation
* batch
* iodine
* selenium

Il y a un effet batch sur 2 TH, un effet iodine pour une et un effet selenium a p = 0.14 sur une (plus d'infos + plots qd andres aura envoyé son doc). Du coup si on fait comme pour les PE on ajoute dans le modèle batch, selenium et iodine. **Qu'en penses-tu?**

## Suite des analyses

Quand j'ai vu rémy pour notre point croissance, nous avons également discuté du stage d'andres. Il pense que nous nous y prenons mal. La partie ED/TH est pour lui une analyse à part entière, séparée de la médiation ou l'on peut utiliser des modèles "fancy" si on veut. Mais que si on veut faire l'analyse de médiation, pas besoin de faire tout ça. Lui partirait juste sur peu de composés mis en avant lors de l'analyse d'ariane et faire une analyse de médiation classique avec régression linéraire avec et sans les TH et regarder le % de var expliquée. Il a l'air de dire soit andres fait les 2 séparément, soit il se focalise sur ED/TH. Selon Rémy il s'agit de 2 papiers distincts.

Les deux options sont donc:

1. On focalise sur ED/TH et on fait une méthode plus fancy type BKMR et on fait la médiation plus tard
2. On se dit que Andres va faire les 2, et on fait de la régression linéaire partout avec une analyse ED/TH et une analyse mediation limitée aux composés d'Ariane (Rémy avait l'air plutôt en faveur de cette option)

Je n'ai pas de préférence et Andres semble préférer la 1. mais on te laisse trancher.

**Next steps:**

* Décider le choix précédent
* choix du modèle final
* rédiger plan d'analyse

Ah oui, que faire des 3 composés non mesurés au batch 1? Soit on enlève ces composés, soit on perd 10 lignes soit on impute.

# Growth

J'ai quasi fini les résultats. Il me reste à tout mettre au propre. Je te ferais un résumé des résultats pour la prochaine fois.

Il me reste à investiguer les données manquantes (notamment sur les outcomes), Rémy avait l'air inquiet qu'on en ait autant. J'ai un nouveau point avec Rémy dans un mois.

